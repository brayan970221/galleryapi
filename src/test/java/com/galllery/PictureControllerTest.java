package com.galllery;



import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


import java.io.IOException;
import java.security.SecureRandom;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gallery.GalleryApplication;
import com.gallery.models.Picture;
import com.gallery.utils.TestExpectException;
import com.google.gson.Gson;


@SpringBootTest(classes = GalleryApplication.class)
@CrossOrigin(allowedHeaders = "*", allowCredentials = "*")
public class PictureControllerTest {
	
	private static final String[] KEY = { "ID", "TITLE", "TOTAL_QUALIFICATIONS", "NUMBER_QUALIFICATIONS", "IMAGE" };
	
	@Autowired
	WebApplicationContext webApplicationContext;
	@Autowired
	private Gson gson = new Gson();	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private MockMvc mockMvc;		
	private SecureRandom rdm = new SecureRandom();	


	@BeforeEach
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}	
	

	/**
	 * Integration test for Create Picture operation
	 * 
	 * @throws TestExpectException This exception occurs when the expected conditions in a test do not fulfilled
	 * @throws SQLException 
	 */
	@Test	
	@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, statements = { "TRUNCATE TABLE picture" })
	@Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD, statements = { "TRUNCATE TABLE picture" })
	public void testCreatePicture() throws TestExpectException, SQLException {
		Picture picture = new Picture();
		byte[] data = new byte[1000];
		rdm.nextBytes(data);	
		picture.setId(String.valueOf(rdm.nextInt(1000)));
		picture.setTitle("Title "+ String.valueOf(rdm.nextInt(100)));
		picture.setNumberQualifications(rdm.nextInt(50));
		picture.setTotalQualifications(rdm.nextInt(1000));
		char[] my = {'a','b'};
		picture.setImage(my);
		try {
			mockMvc.perform(post("/Picture/create")
					.contentType(MediaType.APPLICATION_JSON)
					.content(gson.toJson(picture))
					.characterEncoding("utf-8"))
					.andExpect(status().isOk())
					.andExpect(content().json("{\"msg\":\"Saved successfully\"}"))
					.andReturn();			
			verifyPicture(picture, null , jdbcTemplate.queryForList("SELECT * FROM picture WHERE id= "+picture.getId()), 0);		
		} catch (Exception e) {
			throw new TestExpectException(e.getMessage());
		}
	}
	
	/**
	 * Integration test for readAllPictures 
	 * 
	 * @throws TestExpectException This exception occurs when the expected conditions in a test do not fulfilled
	 */
	@Test
	@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, statements = { "TRUNCATE TABLE picture",
			"insert into picture (number_qualifications, title, total_qualifications, id, image) values (5, 'title1', 60, '2', 'TEST')",
			"insert into picture (number_qualifications, title, total_qualifications, id, image) values (6, 'title2', 20, '3', 'TEST2')",
			"insert into picture (number_qualifications, title, total_qualifications, id, image) values (3, 'title3', 30, '4', 'TEST3')"})
	@Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD, statements = { "TRUNCATE TABLE picture" })
	public void readAllPictureTest() throws TestExpectException {
		try {
			MvcResult result = mockMvc.perform(get("/Picture/read"))
					.andExpect(status().isOk())
					.andReturn();					
			ObjectMapper mapper = new ObjectMapper();
			List<Map<String, Object>> picturesAPI = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<Map<String, Object>>>(){});		
			List<Map<String, Object>> picturesDB = jdbcTemplate.queryForList("SELECT * FROM picture");
			Assert.assertEquals("Read data size does not match", picturesDB.size() , picturesAPI.size());
			for (int i = 0; i < picturesDB.size(); i++) {
				verifyPicture(null,picturesAPI, picturesDB, i);							
			}		
		} catch (Exception e) {
			throw new TestExpectException(e.getMessage());
		}		
	}
	
	/**
	 * Integration test for readPictureById 
	 * 
	 * @throws TestExpectException This exception occurs when the expected conditions in a test do not fulfilled
	 */
	@Test
	@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, statements = { "TRUNCATE TABLE picture",
			"insert into picture (number_qualifications, title, total_qualifications, id, image) values (5, 'title1', 60, '2', 'TEST')",
			"insert into picture (number_qualifications, title, total_qualifications, id, image) values (6, 'title2', 20, '3', 'TEST2')",
			"insert into picture (number_qualifications, title, total_qualifications, id, image) values (3, 'title3', 30, '4', 'TEST3')"})
	@Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD, statements = { "TRUNCATE TABLE picture" })
	public void readPictureByIdTest() throws TestExpectException {
		try {
			MvcResult result = mockMvc.perform(get("/Picture/read/{id}", "3"))
					.andExpect(status().isOk())
					.andReturn();					
			ObjectMapper mapper = new ObjectMapper();
			Map<String, Object> picture = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Map<String, Object>>(){});
			List<Map<String, Object>> picturesAPI = new ArrayList<>();
			picturesAPI.add(picture);
			List<Map<String, Object>> picturesDB = jdbcTemplate.queryForList("SELECT * FROM picture WHERE id ='"+3+"'");
			verifyPicture(null,picturesAPI, picturesDB, 0);				
		} catch (Exception e) {
			throw new TestExpectException(e.getMessage());
		}		
	}
	
	/**
	 * Integration test for readPictureById 
	 * 
	 * @throws TestExpectException This exception occurs when the expected conditions in a test do not fulfilled
	 */
	@Test
	@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, statements = { "TRUNCATE TABLE picture",
			"insert into picture (number_qualifications, title, total_qualifications, id, image) values (5, 'title1', 60, '2', 'TEST')",
			"insert into picture (number_qualifications, title, total_qualifications, id, image) values (6, 'title2', 20, '3', 'TEST2')",
			"insert into picture (number_qualifications, title, total_qualifications, id, image) values (3, 'title3', 30, '4', 'TEST3')"})
	@Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD, statements = { "TRUNCATE TABLE picture" })
	public void deletePictureByIdTest() throws TestExpectException {
		try {
			mockMvc.perform(delete("/Picture/delete/{id}", "3"))
					.andExpect(status().isOk())
					.andExpect(content().json("{\"msg\":\"Deleted successfully\"}"))
					.andReturn();				
			List<Map<String, Object>> picturesDB = jdbcTemplate.queryForList("SELECT * FROM picture WHERE id ='"+3+"'");
			Assert.assertEquals("City was not deleted successfully", 0, picturesDB.size());			
		} catch (Exception e) {
			throw new TestExpectException(e.getMessage());
		}		
	}
	
	/**
	 * Integration test for Edit Picture operation
	 * 
	 * @throws TestExpectException This exception occurs when the expected conditions in a test do not fulfilled
	 * @throws SQLException 
	 */
	@Test	
	@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, statements = { "TRUNCATE TABLE picture",
			"insert into picture (number_qualifications, title, total_qualifications, id, image) values (5, 'title1', 60, '2', 'TEST')",
			"insert into picture (number_qualifications, title, total_qualifications, id, image) values (6, 'title2', 20, '3', 'TEST2')",
			"insert into picture (number_qualifications, title, total_qualifications, id, image) values (3, 'title3', 30, '4', 'TEST3')"})
	@Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD, statements = { "TRUNCATE TABLE picture" })
	public void testEditPicture() throws TestExpectException, SQLException {
		Picture picture = new Picture();
		picture.setId("3");
		picture.setTitle("Title "+ String.valueOf(rdm.nextInt(100)));
		picture.setNumberQualifications(rdm.nextInt(50));
		picture.setTotalQualifications(rdm.nextInt(1000));
		char[] my = {'a','b'};
		picture.setImage(my);
		try {
			mockMvc.perform(put("/Picture/edit")
					.contentType(MediaType.APPLICATION_JSON)
					.content(gson.toJson(picture))
					.characterEncoding("utf-8"))
					.andExpect(status().isOk())
					.andExpect(content().json("{\"msg\":\"Saved successfully\"}"))
					.andReturn();			
			verifyPicture(picture, null , jdbcTemplate.queryForList("SELECT * FROM picture WHERE id= "+picture.getId()), 0);		
		} catch (Exception e) {
			throw new TestExpectException(e.getMessage());
		}
	}
	

	
	/**
	 * Verify if the data in the database is equal to the data that return the API
	 * 
	 * @param pictureRead Picture object Mapped
	 * @param pictureCreate   Picture object in the API
	 * @param pictureDB Picture object in database
	 * @param index  Index in the List returned by the database, if its an only
	 *               element index 0 by default
	 * @throws SQLException 
	 * @throws IOException 
	 */
	public void verifyPicture(Picture pictureCreate, List<Map<String, Object>> pictureRead ,List<Map<String, Object>> pictureDB, Integer index) throws SQLException, IOException {
		Assert.assertEquals(KEY[0],(pictureCreate == null) ? pictureRead.get(index).get("id").toString() : pictureCreate.getId().toString()
				, pictureDB.get(index).get(KEY[0]).toString());
		Assert.assertEquals(KEY[1], (pictureCreate == null) ? pictureRead.get(index).get("title").toString() : pictureCreate.getTitle().toString()
				, pictureDB.get(index).get(KEY[1]).toString());
		Assert.assertEquals(KEY[2], (pictureCreate == null) ? pictureRead.get(index).get("totalQualifications").toString() : pictureCreate.getTotalQualifications().toString()
				, pictureDB.get(index).get(KEY[2]).toString());
		Assert.assertEquals(KEY[3], (pictureCreate == null) ? pictureRead.get(index).get("numberQualifications").toString() : pictureCreate.getNumberQualifications().toString()
				, pictureDB.get(index).get(KEY[3]).toString());
		Assert.assertEquals(KEY[4], (pictureCreate == null) ? pictureRead.get(index).get("image").toString() : new String(pictureCreate.getImage())
				, pictureDB.get(index).get(KEY[4]).toString());
		 
	}

}
