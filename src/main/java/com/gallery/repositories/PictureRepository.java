package com.gallery.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gallery.models.Picture;

public interface PictureRepository extends JpaRepository<Picture, String> {

}
