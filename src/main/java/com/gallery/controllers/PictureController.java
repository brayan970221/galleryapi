package com.gallery.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
//import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.gallery.models.Picture;
import com.gallery.models.Response;
import com.gallery.services.PictureIService;

@RestController
//@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/Picture")
@EnableWebMvc
public class PictureController {

	@Autowired
	PictureIService pictureService;

	@PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	Response createPicture(@RequestBody Picture picture) {
		return new Response(pictureService.savePicture(picture));
	}

	@GetMapping(value = "/read", produces = MediaType.APPLICATION_JSON_VALUE)
	List<Picture> readAllPictures() {
		return pictureService.readAllPictures();
	}

	@GetMapping(value = "/read/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	Picture readPictureById(@PathVariable("id") String id) {
		return pictureService.readPictureById(id);
	}

	@DeleteMapping(value = "/delete/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	Response deletePicture(@PathVariable("id") String id) {
		return new Response(pictureService.deletePicture(id));
	}

	@PutMapping(value = "/edit", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	Response editPicture(@RequestBody Picture picture) {
		return new Response(pictureService.savePicture(picture));
	}
}
