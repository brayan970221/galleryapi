package com.gallery.models;

import lombok.Data;

@Data
public class Response {
	
	private String msg;
	
	public Response (String message) {
		this.msg= message;
	}

}
