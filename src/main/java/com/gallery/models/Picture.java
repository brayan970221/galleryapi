package com.gallery.models;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;

import lombok.Data;

@Entity
@Data
public class Picture {
	@Id
	private String id;
	@Column
	private String title;
	@Column
	private Integer totalQualifications;
	@Column
	private Integer numberQualifications; 
	@Lob
	@Column(name = "image", columnDefinition="clob")
	private char[] image;
}
