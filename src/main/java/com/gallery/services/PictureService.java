package com.gallery.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gallery.models.Picture;
import com.gallery.repositories.PictureRepository;

@Service
public class PictureService implements PictureIService {
	
	@Autowired
	private PictureRepository pictureRepository;
	
	/**
	 * Returns a list with all the pictures in the database
	 * 
	 * @return List of pictures
	 */
	public List<Picture> readAllPictures() {
		return pictureRepository.findAll();
	}
	
	/**
	 * Returns a picture for the id entered
	 * 
	 * @param id String that represents the id of the picture
	 * @return Picture for the id entered
	 */
	public Picture readPictureById(String id) {
		try {
			return pictureRepository.findById(id).get();
		}
		catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * Create a picture register in the database
	 * 
	 * @param picture Picture Object that will be saved in the database
	 */
	public String savePicture(Picture picture) {
		pictureRepository.save(picture);
		return "Saved successfully";
	}
	
	/**
	 * Delete the picture for the id entered
	 * 
	 * @param id String that represents the id of the picture
	 */
	public String deletePicture(String id) {
		pictureRepository.deleteById(id);
		return "Deleted successfully";
	}
	

}
