package com.gallery.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.gallery.models.Picture;

@Service
public interface PictureIService {
	
	/**
	 * Returns a list with all the pictures in the database
	 * 
	 * @return List of pictures
	 */
	List<Picture> readAllPictures();
	
	/**
	 * Returns a picture for the id entered
	 * 
	 * @param id String that represents the id of the picture
	 * @return Picture for the id entered
	 */
	Picture readPictureById(String id);
	
	/**
	 * Create a picture register in the database
	 * 
	 * @param picture Picture Object that will be saved in the database
	 * @return Message with the response for the operation
	 */
	String savePicture(Picture picture);
	
	/**
	 * Delete the picture for the id entered
	 * 
	 * @param id String that represents the id of the picture
	 * @return Message with the response for the operation
	 */
	String deletePicture(String id);

}
